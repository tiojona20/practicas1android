package com.example.myapplication;

public class ItemsData {
    private String txtCategoria;
    private String txtDescripcion;
    private int imageId;

    public ItemsData(){
        this.txtCategoria = "";
        this.txtDescripcion = "";
        this.imageId = 0;
    }

    public ItemsData(String txtCategoria, String txtDescripcion, int imageId) {
        this.txtCategoria = txtCategoria;
        this.txtDescripcion = txtDescripcion;
        this.imageId = imageId;
    }

    public ItemsData(ItemsData items) {
        this.txtCategoria = items.txtCategoria;
        this.txtDescripcion = items.txtDescripcion;
        imageId = items.imageId;
    }

    public String getTxtCategoria() {
        return txtCategoria;
    }

    public void setTxtCategoria(String txtCategoria) {
        this.txtCategoria = txtCategoria;
    }

    public String getTxtDescripcion() {
        return txtDescripcion;
    }

    public void setTxtDescripcion(String txtDescripcion) {
        this.txtDescripcion = txtDescripcion;
    }

    public int getImageId() {
        return imageId;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }



}
